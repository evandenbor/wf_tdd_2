# TDD Opdracht 2

Tweede opdracht Test Driven Development voor Testen en Uitvoeren

## Set-up

* Installeer [Node.js](https://nodejs.org/en/) en [Git](https://git-scm.com/)
* Open een terminal (cmd) in een gewenste map (shift + rechtsklik in Windows verkenner, open opdrachtvenster hier)
* Typ het volgende:
  * git clone https://evandenbor@bitbucket.org/evandenbor/wf_tdd_2.git
  * cd WF_TDD_2
  * npm install
  
## Opdracht

Ontwikkel met behulp van Test Driven Development en Node.js een systeem voor het berekenen van de prijs van een bioscoopticket.
De basisprijs van het ticket is �10. Er zijn een aantal kortingsmogelijkheden:

* 20 % korting voor kinderen van 15 jaar of jonger en ouderen van 65 jaar en ouder
* 10% korting (bovenop de leeftijdskorting) bij invoeren van de kortingscode "geheim"

De applicatie bevat de volgende functions:

* calculateAgeDiscount(age)
* calculateCodeDiscount(code)
* calculateTotalPrice(age, code)

Schrijf eerste calculateAgeDiscount en calculateCodeDiscount. Werk volgens TDD, dus schrijf eerst
unit tests m.b.v. Mocha (zoals je hebt geleerd bij opdracht 1)

**Je hoeft geen user interface te maken, enkel de functions**